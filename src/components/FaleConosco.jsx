import { Header } from "./components/Header"
import { Footer } from './components/Footer'
import { Contato } from "./components/Contato";
import 'global.css'

function FaleConosco ( ){
  return(
    <div className="Background">
    <div className="Background-Opacity">
      <Header />
      <Contato />
    </div> 
    <div> 
      <Footer />
    </div>
    </div>
  )
}

export default FaleConosco;