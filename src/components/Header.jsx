import styles from "./header.module.css"
import { NavLink } from 'react-router-dom'
import React from 'react';

export function Header() {
    return(
        <div className={styles.header}>
            <div className={styles.logo}>
                <img src="src\Assets\Logo.svg"/>
            </div>
            <div className={styles.headerbuttons}>
                <NavLink to="/filmes"><img src="src/assets/Icone-Filmes.svg" /></NavLink>
                <NavLink to="/login"><img src="src/assets/Icone-Entrar.svg" /></NavLink>
                <NavLink to="/faleConosco"><img src="src/assets/Icone-Ajuda.svg" /></NavLink>
            </div>
        </div>
    )
}