import { BrowserRouter, Routes, Route } from "react-router-dom"
import { Home } from "./Pages/home";
import { Register } from "./Pages/Register"
import { MostrarSessoes } from "./Pages/MostrarSessoes"
import { Checkout } from "./Pages/Checkout"
import { Login } from "./Pages/Login"
import { Filmes } from "./Pages/Filmes"
import React from "react";



export function App ( ){
  return ( 
    <BrowserRouter>
      <Routes>
        <Route path ="/" element={<Home />} />
        <Route path ="/FaleConosco" element={<FaleConosco />} />
        <Route path ="/Register" element={<Register />} />
        <Route path ="/MostrarSessoes" element={<MostrarSessoes />} />
        <Route path ="/Checkout" element={<Checkout />} />
        <Route path ="/Login" element={<Login />} />
        <Route path ="/Filmes" element={<Filmes/>} />       
      </Routes>
    </BrowserRouter>
  )
}
