import Styles from './introducao.module.css'
import React from 'react';

export function Introducao (){
  return (
    <div className={Styles.Txt}>
      <img className={Styles.img} src="src/Assets/Assets/Fundo.svg" ></img>
      <p className={Styles.titulo}>Transformando Filmes em Experiências <span>Personalizadas</span></p>
      <p className={Styles.subtxt}>Reserve Seu Assento e Viva a Magia do Cinema!</p>
    </div>

  )
}
