import "./slider.module.css"
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/swiper-bundle.css'; 


export function Slider() {
  const data = [
    {
      id : "1",
      titulo: "Pipoca (P)",
      preco: "R$ 11,99",
      descricao: 'Nada diz "cinema" como o aroma inconfundível de pipoca estourando. E quando se trata de uma porção de pipoca pequena, a magia se torna ainda mais irresistível. Imagine uma explosão de sabores em miniatura, onde cada grão é um pedacinho de felicidade.',
      urlImage: "src/Assets/Assets/Ticket.svg"
    },
    {
      id : "2",
      titulo: "Pipoca (G)",
      preco: "R$ 13,99",
      descricao: "No reino da experiência cinematográfica, uma rainha reina supremamente: a pipoca grande, uma verdadeira protagonista em cada tela de prata. Ela transcende o mero lanche e se transforma em uma jornada culinária que rivaliza com as tramas mais emocionantes.",
      urlImage: "src/Assets/Assets/Ticket2.svg"
    },
    {
      id : "3",
      titulo: "Duo Pipoca (M) + Refri",
      preco: "R$ 11,99",
      descricao: 'Nada diz "cinema" como o aroma inconfundível de pipoca estourando. E quando se trata de uma porção de pipoca pequena, a magia se torna ainda mais irresistível. Imagine uma explosão de sabores em miniatura, onde cada grão é um pedacinho de felicidade.',
      urlImage: "src/Assets/Assets/Ticket3.svg"
    }
  ];

  return (
    <Swiper
      slidesPerView={1}
      pagination={{ clickable: true }}
      navigation
    >
      {data.map((item) => (
        <SwiperSlide key={item.id}>
          <img
            src={item.urlImage}
            alt="Slider"
            className="slide-item"
          />
        </SwiperSlide>
      ))}
    </Swiper>
  );
}