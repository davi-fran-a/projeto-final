import Styles from './cartaz.module.css'
import React from 'react';

export function Cartaz (){
  return (
    <section>

    <div className={Styles.TituloExposicao}>
    <h1 className={Styles.central}> Em Cartaz </h1>
    </div>

    <div className={Styles.filmes}>
   
      <div>
        <img className={Styles.Filme} src="src\Assets\Assets\Capa.svg"></img>
        <p className={Styles.NomeFilme}>Besouro Azul</p>
        <button className={Styles.section}>SESSÕES DISPONÍVEIS</button>
      </div>

      <div> 
        <img className={Styles.Filme2} src="src\Assets\Assets\Capa (1).svg"></img>
        <p className={Styles.NomeFilme}>Barbie</p>
        <button className={Styles.section}>SESSÕES DISPONÍVEIS</button>
      </div>
      
      <div>
        <img className= {Styles.Filme3} src="src\Assets\Assets\Capa (2).svg"></img>
        <p className={Styles.NomeFilme}>Missão: Impossível</p>
        <button className={Styles.section}>SESSÕES DISPONÍVEIS</button>
      </div>

      <div>
        <img className={Styles.Filme4} src="src\Assets\Assets\Capa (3).svg"></img>
        <p className={Styles.NomeFilme}>Oppenheimer</p>
        <button className={Styles.section}>SESSÕES DISPONÍVEIS</button>
      </div>

      <div>
        <img className= {Styles.Filme5} src="src\Assets\Assets\Capa (4).svg"></img>
          <p className={Styles.NomeFilme}>Elementos</p>
        <button className={Styles.section}>SESSÕES DISPONÍVEIS</button>
        <div>
          <a className={Styles.link} href="">Ver mais</a>
        </div>
        
      </div>
      </div>
      
      </section>
  )
}