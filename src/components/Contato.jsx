import Styles from './contato.module.css';
import "../global.css"
import React from 'react';

export function Contato() {
  return (
    <div className={Styles.Container}>
      <div className={Styles.Blocos}>

        <div className={Styles.Textos}>
          <p className={Styles.Titulo_Container}>Contato</p>
          <p className={Styles.Subtitulo_Container}>Encontrou algum problema?</p>
          <p className={Styles.Sub_Container}>Envie uma mensagem!</p>
        </div>
        <div className={Styles.Nome}
        >
          
          <input type="text" className={Styles.Button1} placeholder='Nome completo'/>
        </div>
        <div className={Styles.Assunto}>
          <input type="text" className={Styles.Button2} placeholder='Assunto'/>
        </div>
        <div className={Styles.Textarea}>
          <textarea className={Styles.Mensagem} placeholder='Descrição detalhada'></textarea>
        </div>
        <div className={Styles.Enviar}>
          <button className={Styles.Enviando}>Enviar</button>
        </div>
      </div>
    </div>
  );
}
