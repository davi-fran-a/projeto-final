import styles from "./card.module.css"
import "../global.css"
import React from 'react'
import { NavLink } from "react-router-dom";

export function Card({ titulo, imgurl, sinopse, genero, classificacao, diretor}){
    
    const maxLength = 150;

    const truncatedSinopse = sinopse.length > maxLength
        ? `${sinopse.substring(0, maxLength)}...`
        : sinopse;

    return(
        <div className={styles.card}>
            <div className={styles.capa}>
                <img src={imgurl} alt="Capa do filme" />
            </div>
            <div>
                <div className={styles.titleline}>
                    <div className={styles.titulo}>
                        <p>{titulo}</p>
                    </div>
                    <div className={styles.idade}>
                        <img src={`src/Assets/C${classificacao}.svg`} Alt={classificacao}/>
                    </div>
                </div>
                <div className={styles.classinf}>
                    <p>{genero}</p>
                    <p>Direção: {diretor} </p>
                    <p>{truncatedSinopse}</p>
                </div>
            </div>
            <div className={styles.info}>

            </div>
            <div className={styles.sessoesbutton}>
                <NavLink to="/mostrarsessoes"><button>
                    Ver sessões
                </button></NavLink>
            </div>
        </div>
    )
}