const express = require('express');

const userRoutes = express.Router();

//Inclusao dos middlewares

const requestAllow = require("../middleware/requestAllowMiddleware")
const checkUserExists = require("../middleware/checkUserExistsMiddleware");
//Inclusao dos controllers

const userController = require("../controllers/userController")

//Criar usuario
userRoutes.post('/registro', requestAllow, checkUserExists ,(request,response)=>userController.createUser(request,response))

//Achar usuario por id
userRoutes.get('/:id', requestAllow, (request,response)=> userController.getUser(request,response))

//Listar todos os users 
userRoutes.get('/',requestAllow, (request,response)=> userController.listUser(request,response))
//Login usuario
userRoutes.post("/login",requestAllow, (request,response)=>userController.userLogin(request,response))



module.exports = userRoutes;