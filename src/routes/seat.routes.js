const express = require('express');

const seatRoutes = express.Router();
//Inclusao dos middlewares
const requestAllow = require("../middleware/requestAllowMiddleware")
//Inclusao dos controllers

const seatController = require("../controllers/seatController")

seatRoutes.patch('/', requestAllow ,(request,response)=> seatController.reserveSeat(request,response))

module.exports = seatRoutes