const express = require('express');

const sessionRoutes = express.Router();

const requestAllow = require("../middleware/requestAllowMiddleware");


const sessionController = require("../controllers/sessionController");

sessionRoutes.post('/:id', requestAllow,(request,response)=> sessionController.createSession(request,response))

sessionRoutes.get('/', requestAllow,(request,response)=> sessionController.listSessions(request,response))

sessionRoutes.get("/", requestAllow, (request, response)=> sessionController.listSessions(request,response))

sessionRoutes.get("/:id")

module.exports = sessionRoutes