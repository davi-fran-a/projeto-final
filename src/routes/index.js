const Router = require("express");

const userRoutes = require("./user.routes");
const movieRoutes = require("./movie.routes")
const seatRoutes = require("./seat.routes")
const sessionRoutes = require("./session.routes")
const router = Router();

router.use('/movie', movieRoutes);
router.use('/users', userRoutes);
router.use('/seat', seatRoutes);
router.use('/session', sessionRoutes)

module.exports = router;
