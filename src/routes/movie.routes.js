const express = require('express');

const movieRoutes = express.Router();
//Inclusao dos middlewares 
const requestAllow = require("../middleware/requestAllowMiddleware")
//Inclusao dos controllers
const movieController = require("../controllers/movieController")
const sessionController = require("../controllers/sessionController");
const seatController = require("../controllers/seatController")

movieRoutes.post('/' ,requestAllow ,(request,response)=>movieController.createMovie(request,response))

movieRoutes.get('/', requestAllow, (request,response)=>movieController.listMovie(request,response))


movieRoutes.get('/:id', requestAllow,(request,response)=> seatController.reserveSeat(request,response))

module.exports = movieRoutes;
