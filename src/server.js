//Inicializar servidor via "yarn server", no terminal
const express = require('express')
const swaggerUi = require("swagger-ui-express")
const app = express()
const port = 3333;

const router = require("./routes/index")
const swaggerFile = require("./swagger.json")
const createDatabase = require('./scripts/createDatabase');

app.use(express.json());

//Inicialização do swagger
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerFile))

app.use(router);




app.listen(port)

createDatabase();