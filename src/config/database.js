const { Sequelize } = require("sequelize")

const sequelize = new Sequelize({
    dialect: "sqlite",
    storage: "./data/database.sqlite"
});

//Testa conexão ao Banco de Dados
async function testConnectionDatabase() {
    try {
        await sequelize.authenticate();
    } catch (error) {
        console.log("Não foi possivel se conectar")
    }
};

testConnectionDatabase();

module.exports = sequelize;

