const { Assentos } = require("./src/models/models.js")
const {User, Seat} = require("../models/models.js")
async function verAssento (request, response, next) {
    try {
        const { nome } = request.body;

        const assentoEscolhido = await assentoEncontrar({
            where: {
                nome,
            }
        });

        if (assentoEscolhido) {
            return response.status(400).json({error: "Assento Indisponível"});
        }

        return next();
    } catch(error) {
        return response.status(400).json({error: "Error ao checar assento"});
    }
};

module.exports = {
    verAssento

};
