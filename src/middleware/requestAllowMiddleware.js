//Codigo nescessario para permitir acesso do front end ao back
function allowRequest(request,response,next){
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader(
      "Access-Control-Allow-Methods",
      "OPTIONS, GET, POST, PUT, PATCH, DELETE"
    );
    response.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
    if (request.method === "OPTIONS") {
      return response.sendStatus(200);
    }
    next();
}

module.exports = allowRequest