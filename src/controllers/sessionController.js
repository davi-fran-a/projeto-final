
const { Session, Movie} = require("../models/models");
const Seat = require("./seatController")
const rows = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"]
const columns = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]
//Ao clicar na sessao no site, sera enviada req para criar uma sessao para a page
async function createSession(request, response){
    try {
        const movie = request.params
        const MovieMovieid = movie.id

        const {tempo, cidade, bairro,tipo} = request.body;

        const session = await Session.create({
            tempo,
            cidade,
            bairro,
            tipo,
            //FK
            MovieMovieid
            
        })

        const movieToUpdate = await Movie.findByPk(MovieMovieid);
        if (!movieToUpdate) {
            return response.status(404).json({ error: "Movie not found" });
        }
        movieToUpdate.SessionId = session.Sessionid;
        await movieToUpdate.save();
        
        const seat = Seat.createSeat(rows,columns,session.Sessionid)
        if(!seat){
            console.log(error.name)
            console.log(error.stack)
            console.log(error.message)
            return response.status(400).json({error: "houve um erro na criação dos assentos"})
        }

        return response.status(201).json(session)
    }catch(error){
        console.log(error.name)
        console.log(error.stack)
        console.log(error.message)
        return response.status(400).json({error: "houve um erro na criação da sessao"})
    }
}

async function listMovieSessions(request,response){
    const {movieid} = request.body
    try{
        const session = await Session.findAll({
            where: {
                MovieMovieid : movieid
            }
        })
        if(!session){
            return response.status(400).json({error: "nao foi possivel encontrar a sessao"})
        }
        return response.status(200).json(session)
    }catch(error){
        return response.status(400).json({error: "houve um erro ao buscar a sessao"})
    }
}
async function listSessions(request,response){
    try {
        
        const session = await Session.findAll();
        return response.status(200).json(session);

    }catch(error){

        return response.status(400).json({error: "Nao foram encontradas sessoes"})

    }
}

module.exports = {
    createSession,
    listSessions,
    listMovieSessions
}