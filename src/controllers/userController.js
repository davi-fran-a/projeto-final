
const { v4:uuidv4 } = require('uuid');
const {Op} = require('sequelize')
const {User} = require('../models/models');
const { loadConfigFromFile } = require('vite');
const usersBase = [];
//Criar usuario

async function createUser(request,response){
    try {
        console.log("solicitado criação de user")
        //Recebe as informações do cadastro em variaveis
        const {nome, sobrenome, cpf, datanasc, username, email, password} = request.body
        const user = await User.create({
            nome,
            sobrenome,
            cpf,
            datanasc,
            username,
            email,
            password
        })

        return response.status(201).json(user);
    }catch(error){
        return response.status(400).json({error: "nao foi possivel criar o usuario solicitado"})
    }
}

async function getUser(request,response){
    try {
        //Recebe as informações do cadastro em variaveis
        const { id } = request.params
        const user = await User.findOne({
            where: {
                Userid: id
                }
            })
        return response.status(200).json(user);
    }catch(error){
        return response.status(400).json({error: "nao foi possivel encontrar o usuario solicitado"})
    }

}

async function listUser(request,response){
    try {
        //Recebe as informações do cadastro em variaveis
        const user = await User.findAll()
        return response.status(201).json(user);
    }catch(error){

        return response.status(400).json({error: "nao foi possivel encontrar o usuario solicitado"})
    }

}

async function userLogin(request, response) {
    const { username, email, password } = request.body;
    try {
        const user = await User.findOne({
            where: {
                [Op.or]: [
                    { username: username || "" },
                    { email: email || "" }
                ]
            }
        });

        if (!user) {
            return response.status(400).json({ error: "Não foi possível encontrar o usuário solicitado!" });
        }

        // Compare a senha somente se um usuário for encontrado
        console.log("logue senha e user")
        const loguser = await User.findOne({
            where: {
                [Op.and]: [
                    { username: username },
                    { password: password }
                ]
            }
        });

        if (!loguser) {
            console.log("user nao existe, tente email")
            const loguser = await User.findOne({
                where: {
                    [Op.and]: [
                        { email: email },
                        { password: password }
                    ]
                }
            });
            if(!loguser){
                return response.status(400).json({ error: "Senha incorreta!" });
            }
            console.log("logado")
            return response.status(200).json(user.id);
        }
        console.log("logado")
        // Se a senha corresponder, retorne 200
        return response.status(200).json({message: "Usuario logado com sucesso"});
    } catch (error) {
        console.log(error.name);
        console.log(error.stack);
        console.log(error.message);
        return response.status(400).json({ error: "Houve um erro ao realizar o login" });
    }
}


module.exports = {
    createUser,
    getUser,
    listUser,
    userLogin
}