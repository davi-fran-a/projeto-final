const {Movie} = require('../models/models');
//Criar filme

async function createMovie(request,response){
    try {
        console.log("solicitado criação de filme")
        //Recebe as informações do cadastro de filmes em variaveis
        const {
            titulo,imgurl,sinopse,genero,classificacao,diretor} = request.body

        const movie = await Movie.create({
            titulo,
            imgurl,
            sinopse,
            genero,
            classificacao,
            diretor
        })
        return response.status(201).json(movie);
    }catch(error){
        console.log(error.stack)
        return response.status(400).json({error: "nao foi possivel criar o filme solicitado"})
    }
}
async function listMovie(request,response){
    try {
        //Recebe as informações do cadastro em variaveis
        const movies = await Movie.findAll()
        return response.status(200).json(movies);
    }catch(error){
        return response.status(400).json({error: "houve um erro ao procurar pelos filmes"})
    }

}

module.exports = {
    createMovie,
    listMovie
}