const {Op} = require("sequelize")
const {Seat, User} = require("../models/models")
const { response } = require("express")
const { userLogin } = require("./userController")
const cpf = 0
const nome = ""

//Criar assentos assim que uma sessao é criada, chamado por createSession no sessionController.js

async function createSeat(rows,columns, sessionId){
    //Para cada fileira, crie as cadeiras
    rows.forEach((row) => {
        try {
            //Para cada coluna(numero) faça uma fileira
            columns.forEach(async (column) => {
                const seat = await Seat.create({
                    numero: column,
                    fileira: row,
                    preco: "14.00",
                    cpf: cpf,
                    nome: nome,
                    SessionSessionid: sessionId
                })
            return seat
            })
        }catch(error){
            return
        }
    })

}

//Reservar lugares

//Luciene vai terminar esta parte, lembrar de revisar
async function reserveSeat(request,response){
    const { UserId, sessionid, numero, fileira } = request.body
    try {
        console.log("reserva")
        const user = await User.findOne ({
            where: {
                UserId
            }   
        })
        if(!user){
            return response.status(400).json({error: "usuario nao encontrado"})
        }
        const seat = await Seat.findOne({
            where: {
                numero,
                fileira
            }
        })
        if(seat.cpf != 0){
            return response.status(400).json({error: "cpf ja cadastrado neste assento"})
        }
        seat.cpf = user.cpf
        seat.nome = user.nome
        return response.status(200).json(seat)
    }catch(error){
        console.log(error.name)
        console.log(error.message)
        console.log(error.stack)
        return response.status(400).json({error: "houve um erro ao reservar a assento"})
    }
}

module.exports = {
    createSeat,
    reserveSeat
}