import { Footer } from "../components/Footer"
import { Header } from "../components/Header"
import styles from "./register.module.css"
import "./global.css"
import React, { useState } from 'react';

export function Register() {
    const [formData, setFormData] = useState({
        nome: '',
        sobrenome: '',
        cpf: '',
        datanasc: '',
        username: '',
        email: '',
        password: ''
    });

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleRegistration = async (e) => {
        e.preventDefault();

        try {
            const response = await fetch('http://localhost:3333/users/registro', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(formData),
            });

            if (response.ok) {
                // Registro bem-sucedido, redirecionar ou executar ações necessárias
                console.log('Registro bem-sucedido!');
            } else {
                console.error('Erro ao registrar usuário:', response.statusText);
            }
        } catch (error) {
            console.error('Erro ao processar a solicitação:', error);
        }
    };
    return (
        <div className={"Background"}>
            <div className={"Background-Opacity"}>
                <div>  
                    <Header />
                        <div>
                            <main className={styles.register}>
          
                                <div className={styles.call}>
                                    <p>
                                        Junte-se à Comunidade Cinematográfica!
                                        Cadastre-se Aqui!
                                    </p>
                                </div>
                                <div className={styles.calltext}>
                                    <p>
                                        Seja bem-vindo à nossa comunidade apaixonada pelo mundo do cinema. Ao fazer parte do nosso espaço digital, você está prestes a mergulhar em uma experiência cinematográfica única, onde a magia das telonas ganha vida com um toque moderno.
                                    </p>
                                    <p>
                                        Nosso formulário de cadastro é o primeiro passo para embarcar nessa jornada emocionante. Ao preenchê-lo, você se tornará um membro da nossa comunidade, onde amantes do cinema se reúnem para compartilhar o entusiasmo, as emoções e as histórias que permeiam cada cena.
                                    </p>
                                </div>
                                <form className={styles.regform} onSubmit={handleRegistration}>
                                    <h1>Registre-se</h1>     
                                    <input type="text" name="nome" placeholder="Nome"/>
                                    <input type="text" name="sobrenome" placeholder="Sobrenome"/>
                                    <input type="text" name="cpf" placeholder="CPF"/>
                                    <input type="text" name="datanasc" placeholder="Data de nascimento"/>
                                    <input type="text" name="username" placeholder="Nome de Usuário"/>
                                    <input type="email" name="email" placeholder="E-mail"/>
                                    <input type="password" name="password" placeholder="Senha"/>
                                    <input type="password" name="password" placeholder="Confirmar senha"/>
                                    <button className={styles.regisbutton} type="submit">REGISTRAR</button>
                                </form>
                            </main>
                        </div>
                    <Footer/>
                </div>
            </div>
        </div>
    )
}
