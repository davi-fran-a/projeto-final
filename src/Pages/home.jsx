import React from 'react';
import './global.css'
import { Header } from '../components/Header';
import { Footer } from '../components/Footer';
import { Introducao } from '../components/Introducao'
import { Slider } from '../components/Slider'
import { Cartaz } from '../components/Cartaz'

export function Home (){
  return(
    <div className="Background">
      <div className="Background-Opacity">
        <div>
        <Header />
        <Introducao />
      </div>
      <div>
        <Slider />
      </div>
      <div>
        <Cartaz />
        <Footer />
        </div>
        </div>
      </div>
    
  )
}

