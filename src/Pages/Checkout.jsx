import { Footer } from '../components/Footer'
import { Header } from '../components/Header'
import resets from '../_resets.module.css';
import { BackgroundIcon } from './BackgroundIcon';
import classes from './Checkout.module.css';
import "./global.css"
import React from 'react';
import seats from './src/controllers/seats.js';



export function Checkout(){}
  return (
    <><div className={`${resets.storybrainResets} ${classes.root}`}>
      <div className={classes.background}>
        <BackgroundIcon className={classes.icon} />
      </div>
      <Footer />
      <div className={classes.fundo}></div>
      <div className={classes.legenda}>Legenda</div>
      <div className={classes.frame21}>
        <div className={classes.cadeira1}></div>
      </div>
      <div className={container}>
        <div className={classes.disponivel}>Disponível</div>
        <div className={classes.cadeira12}></div>
        <div className={classes.comprado} class="occupied">Comprado</div>
        <div className={classes.cadeira13}></div>
        <div className={classes.selecionado} class="selected">Selecionado</div>
        <div className={classes.line1}></div>
        <div className={classes.a}>a</div>
        <div className={classes.b}>b</div>
        <div className={classes.c}>C</div>
        <div className={classes.d}>D</div>
        <div className={classes.e}>e</div>
        <div className={classes.f}>f</div>
        <div className={classes.g}>g</div>
        <div className={classes.h}>h</div>
        <div className={classes.i}>i</div>
        <div className={classes.j}>j</div>
        <div className={classes.a2}>a</div>
        <div className={classes.b2}>B</div>
        <div className={classes.c2}>C</div>
        <div className={classes.d2}>D</div>
        <div className={classes.e2}>E</div>
        <div className={classes.f2}>f</div>
        <div className={classes.g2}>g</div>
        <div className={classes.h2}>h</div>
        <div className={classes.i2}>i</div>
        <div className={classes.j2}>j</div>
        <div className={classes.frame212}>
          <div className={classes.frame213}>
            <div className={classes.frame24}>
              <div className={classes.cadeira14} id="seat" class="seat"></div>
              <div className={classes.cadeira2} id="seat" class="seat"></div>
              <div className={classes.cadeira3} id="seat" class="seat"></div>
              <div className={classes.cadeira4} id="seat" class="seat"></div>
              <div className={classes.cadeira5} id="seat" class="seat"></div>
              <div className={classes.cadeira6} id="seat" class="seat"></div>
              <div className={classes.cadeira7} id="seat" class="seat"></div>
              <div className={classes.cadeira8} id="seat" class="seat"></div>
              <div className={classes.cadeira9} id="seat" class="seat"></div>
              <div className={classes.cadeira10} id="seat" class="seat"></div>
              <div className={classes.cadeira11} id="seat" class="seat"></div>
              <div className={classes.cadeira122} id="seat" class="seat"></div>
              <div className={classes.cadeira132} id="seat" class="seat"></div>
              <div className={classes.cadeira142} id="seat" class="seat"></div>
              <div className={classes.cadeira15} id="seat" class="seat"></div>
              <div className={classes.cadeira16} id="seat" class="seat"></div>
              <div className={classes.cadeira17} id="seat" class="seat"></div>
              <div className={classes.cadeira18} id="seat" class="seat"></div>
            </div>
          </div>
          <div className={classes.frame30}>
            <div className={classes.cadeira19} id="seat" class="seat"></div>
            <div className={classes.cadeira22} id="seat" class="seat"></div>
            <div className={classes.cadeira32} id="seat" class="seat"></div>
            <div className={classes.cadeira42} id="seat" class="seat"></div>
            <div className={classes.cadeira52} id="seat" class="seat"></div>
            <div className={classes.cadeira62} id="seat" class="seat"></div>
            <div className={classes.cadeira72} id="seat" class="seat"></div>
            <div className={classes.cadeira82} id="seat" class="seat"></div>
            <div className={classes.cadeira92} id="seat" class="seat"></div>
            <div className={classes.cadeira102} id="seat" class="seat"></div>
            <div className={classes.cadeira112} id="seat" class="seat"></div>
            <div className={classes.cadeira123} id="seat" class="seat"></div>
            <div className={classes.cadeira133} id="seat" class="seat"></div>
            <div className={classes.cadeira143} id="seat" class="seat"></div>
            <div className={classes.cadeira152} id="seat" class="seat"></div>
            <div className={classes.cadeira162} id="seat" class="seat"></div>
            <div className={classes.cadeira172} id="seat" class="seat"></div>
            <div className={classes.cadeira182} id="seat" class="seat"></div>
          </div>
          <div className={classes.frame31}>
            <div className={classes.cadeira110} id="seat" class="seat"></div>
            <div className={classes.cadeira23} id="seat" class="seat"></div>
            <div className={classes.cadeira33} id="seat" class="seat"></div>
            <div className={classes.cadeira43} id="seat" class="seat"></div>
            <div className={classes.cadeira53} id="seat" class="seat"></div>
            <div className={classes.cadeira63} id="seat" class="seat"></div>
            <div className={classes.cadeira73} id="seat" class="seat"></div>
            <div className={classes.cadeira83} id="seat" class="seat"></div>
            <div className={classes.cadeira93} id="seat" class="seat"></div>
            <div className={classes.cadeira103} id="seat" class="seat"></div>
            <div className={classes.cadeira113} id="seat" class="seat"></div>
            <div className={classes.cadeira124} id="seat" class="seat"></div>
            <div className={classes.cadeira134} id="seat" class="seat"></div>
            <div className={classes.cadeira144} id="seat" class="seat"></div>
            <div className={classes.cadeira153} id="seat" class="seat"></div>
            <div className={classes.cadeira163} id="seat" class="seat"></div>
            <div className={classes.cadeira173} id="seat" class="seat"></div>
            <div className={classes.cadeira183} id="seat" class="seat"></div>
          </div>
          <div className={classes.frame23}>
            <div className={classes.frame242}>
              <div className={classes.cadeira111} id="seat" class="seat"></div>
              <div className={classes.cadeira24} id="seat" class="seat"></div>
              <div className={classes.cadeira34} id="seat" class="seat"></div>
              <div className={classes.cadeira44} id="seat" class="seat"></div>
              <div className={classes.cadeira54} id="seat" class="seat"></div>
              <div className={classes.cadeira74} id="seat" class="seat"></div>
              <div className={classes.cadeira192} id="seat" class="seat"></div>
              <div className={classes.cadeira84} id="seat" class="seat"></div>
              <div className={classes.cadeira94} id="seat" class="seat"></div>
              <div className={classes.cadeira104} id="seat" class="seat"></div>
              <div className={classes.cadeira114} id="seat" class="seat"></div>
              <div className={classes.cadeira125} id="seat" class="seat"></div>
              <div className={classes.cadeira135} id="seat" class="seat"></div>
              <div className={classes.cadeira145} id="seat" class="seat"></div>
              <div className={classes.cadeira154} id="seat" class="seat"></div>
              <div className={classes.cadeira164} id="seat" class="seat"></div>
              <div className={classes.cadeira174} id="seat" class="seat"></div>
              <div className={classes.cadeira184} id="seat" class="seat"></div>
            </div>
          </div>
          <div className={classes.frame243}>
            <div className={classes.frame244}>
              <div className={classes.cadeira115} id="seat" class="seat"></div>
              <div className={classes.cadeira25} id="seat" class="seat"></div>
              <div className={classes.cadeira45} id="seat" class="seat"></div>
              <div className={classes.cadeira75} id="seat" class="seat"></div>
              <div className={classes.cadeira85} id="seat" class="seat"></div>
              <div className={classes.cadeira95} id="seat" class="seat"></div>
              <div className={classes.cadeira126} id="seat" class="seat"></div>
              <div className={classes.cadeira136} id="seat" class="seat"></div>
              <div className={classes.cadeira146} id="seat" class="seat"></div>
              <div className={classes.cadeira155} id="seat" class="seat"></div>
              <div className={classes.cadeira165} id="seat" class="seat"></div>
              <div className={classes.cadeira175} id="seat" class="seat"></div>
              <div className={classes.cadeira193} id="seat" class="seat"></div>
              <div className={classes.cadeira20} id="seat" class="seat"></div>
              <div className={classes.cadeira21} id="seat" class="seat"></div>
              <div className={classes.cadeira222} id="seat" class="seat"></div>
              <div className={classes.cadeira232} id="seat" class="seat"></div>
              <div className={classes.cadeira185} id="seat" class="seat"></div>
            </div>
          </div>
          <div className={classes.frame25}>
            <div className={classes.frame245}>
              <div className={classes.cadeira116} id="seat" class="seat"></div>
              <div className={classes.cadeira26} id="seat" class="seat"></div>
              <div className={classes.cadeira35} id="seat" class="seat"></div>
              <div className={classes.cadeira46} id="seat" class="seat"></div>
              <div className={classes.cadeira55} id="seat" class="seat"></div>
              <div className={classes.cadeira64} id="seat" class="seat"></div>
              <div className={classes.cadeira76} id="seat" class="seat"></div>
              <div className={classes.cadeira194} id="seat" class="seat"></div>
              <div className={classes.cadeira127} id="seat" class="seat"></div>
              <div className={classes.cadeira137} id="seat" class="seat"></div>
              <div className={classes.cadeira156} id="seat" class="seat"></div>
              <div className={classes.cadeira166} id="seat" class="seat"></div>
              <div className={classes.cadeira176} id="seat" class="seat"></div>
              <div className={classes.cadeira186} id="seat" class="seat"></div>
              <div className={classes.cadeira202} id="seat" class="seat"></div>
              <div className={classes.cadeira212} id="seat" class="seat"></div>
              <div className={classes.cadeira223} id="seat" class="seat"></div>
              <div className={classes.cadeira233} id="seat" class="seat"></div>
            </div>
          </div>
          <div className={classes.frame26}>
            <div className={classes.frame246}>
              <div className={classes.cadeira117} id="seat" class="seat"></div>
              <div className={classes.cadeira27} id="seat" class="seat"></div>
              <div className={classes.cadeira36} id="seat" class="seat"></div>
              <div className={classes.cadeira86} id="seat" class="seat"></div>
              <div className={classes.cadeira96} id="seat" class="seat"></div>
              <div className={classes.cadeira105} id="seat" class="seat"></div>
              <div className={classes.cadeira118} id="seat" class="seat"></div>
              <div className={classes.cadeira128} id="seat" class="seat"></div>
              <div className={classes.cadeira138} id="seat" class="seat"></div>
              <div className={classes.cadeira147} id="seat" class="seat"></div>
              <div className={classes.cadeira157} id="seat" class="seat"></div>
              <div className={classes.cadeira167} id="seat" class="seat"></div>
              <div className={classes.cadeira177} id="seat" class="seat"></div>
              <div className={classes.cadeira187} id="seat" class="seat"></div>
              <div className={classes.cadeira195} id="seat" class="seat"></div>
              <div className={classes.cadeira203} id="seat" class="seat"></div>
              <div className={classes.cadeira213} id="seat" class="seat"></div>
              <div className={classes.cadeira224} id="seat" class="seat"></div>
            </div>
          </div>
          <div className={classes.frame27}>
            <div className={classes.frame247}>
              <div className={classes.cadeira119} id="seat" class="seat"></div>
              <div className={classes.cadeira28} id="seat" class="seat"></div>
              <div className={classes.cadeira37} id="seat" class="seat"></div>
              <div className={classes.cadeira47} id="seat" class="seat"></div>
              <div className={classes.cadeira56} id="seat" class="seat"></div>
              <div className={classes.cadeira65} id="seat" class="seat"></div>
              <div className={classes.cadeira77} id="seat" class="seat"></div>
              <div className={classes.cadeira87} id="seat" class="seat"></div>
              <div className={classes.cadeira97} id="seat" class="seat"></div>
              <div className={classes.cadeira106} id="seat" class="seat"></div>
              <div className={classes.cadeira1110} id="seat" class="seat"></div>
              <div className={classes.cadeira148} id="seat" class="seat"></div>
              <div className={classes.cadeira158} id="seat" class="seat"></div>
              <div className={classes.cadeira168} id="seat" class="seat"></div>
              <div className={classes.cadeira178} id="seat" class="seat"></div>
              <div className={classes.cadeira188} id="seat" class="seat"></div>
              <div className={classes.cadeira196} id="seat" class="seat"></div>
              <div className={classes.cadeira204} id="seat" class="seat"></div>
            </div>
          </div>
          <div className={classes.frame28}>
            <div className={classes.frame248}>
              <div className={classes.cadeira120} id="seat" class="seat"></div>
              <div className={classes.cadeira29} id="seat" class="seat"></div>
              <div className={classes.cadeira38} id="seat" class="seat"></div>
              <div className={classes.cadeira48} id="seat" class="seat"></div>
              <div className={classes.cadeira57} id="seat" class="seat"></div>
              <div className={classes.cadeira66} id="seat" class="seat"></div>
              <div className={classes.cadeira78} id="seat" class="seat"></div>
              <div className={classes.cadeira88} id="seat" class="seat"></div>
              <div className={classes.cadeira98} id="seat" class="seat"></div>
              <div className={classes.cadeira1111} id="seat" class="seat"></div>
              <div className={classes.cadeira129} id="seat" class="seat"></div>
              <div className={classes.cadeira139} id="seat" class="seat"></div>
              <div className={classes.cadeira169} id="seat" class="seat"></div>
              <div className={classes.cadeira179} id="seat" class="seat"></div>
              <div className={classes.cadeira189} id="seat" class="seat"></div>
              <div className={classes.cadeira197} id="seat" class="seat"></div>
              <div className={classes.cadeira205} id="seat" class="seat"></div>
              <div className={classes.cadeira214} id="seat" class="seat"></div>
            </div>
          </div>
          <div className={classes.frame29}>
            <div className={classes.frame249}>
              <div className={classes.cadeira121} id="seat" class="seat"></div>
              <div className={classes.cadeira210} id="seat" class="seat"></div>
              <div className={classes.cadeira39} id="seat" class="seat"></div>
              <div className={classes.cadeira49} id="seat" class="seat"></div>
              <div className={classes.cadeira58} id="seat" class="seat"></div>
              <div className={classes.cadeira67} id="seat" class="seat"></div>
              <div className={classes.cadeira79} id="seat" class="seat"></div>
              <div className={classes.cadeira89} id="seat" class="seat"></div>
              <div className={classes.cadeira99} id="seat" class="seat"></div>
              <div className={classes.cadeira107} id="seat" class="seat"></div>
              <div className={classes.cadeira1112} id="seat" class="seat"></div>
              <div className={classes.cadeira1210} id="seat" class="seat"></div>
              <div className={classes.cadeira1310} id="seat" class="seat"></div>
              <div className={classes.cadeira149} id="seat" class="seat"></div>
              <div className={classes.cadeira159} id="seat" class="seat"></div>
              <div className={classes.cadeira1610} id="seat" class="seat"></div>
              <div className={classes.cadeira1710} id="seat" class="seat"></div>
              <div className={classes.cadeira1810} id="seat" class="seat"></div>
            </div>
          </div>
        </div></div>
      <div className={classes.luz}></div>
      <div className={classes.tela}>

      </div>
      <div className={classes.tela2}>tela</div>
      <div className={classes.fundo2}>

      </div>
      <div className={classes.botao}><input type="submit"></input></div>
      <button onClick target="_blank" className="classes.confirmar">confirmar</button>

      <script></script>
      function alerta {alert(
        <div class="v2_773"><div class="v2_774"></div><div class="v2_775"><span class="v2_776">Confirmação de Reserva!</span><span class="v2_777">Tem certeza de que deseja confirmar a reserva?</span></div><div class="v2_778"></div>)

          function confirmar {alert(<div class="v2_1097"><div class="v2_1098"></div><div class="v2_1099"><div class="v2_1100"></div><div class="v2_1101"></div><div class="v2_1102"></div></div><div class="v2_1103"><span class="v2_1104">Reserva Confirmada!</span><span class="v2_1105">Estamos felizes em tê-lo conosco para essa experiência cinematográfica. Prepare-se para se envolver em uma jornada emocionante na tela grande!</span><span class="v2_1106">Sua reserva foi confirmada com sucesso para a sessão selecionada.</span></div></div>)}

          <div class="v2_779"><span class="v2_780">confirmar</span></div><div class="v2_781"><span class="v2_782">cancelar</span></div></div>)}</div><>
        <div className={classes.frame302}></div><div className={classes.streamlineShoppingCatergoriesC}>
        </div><div className={classes.assentosEscolhidos}>Assentos escolhidos</div><div className={classes.capa}></div><div className={classes.besouroAzul}>Besouro Azul </div><div className={classes.frame4}>
          <div className={classes._1520}>15:20</div>
        </div><div className={classes.frame5}>
          <div className={classes._2D}>2D</div>
        </div></></>)
