import styles from "./filmes.module.css"
import { Header } from "../components/Header"
import { Footer } from "../components/Footer"
import { Card } from "../components/Card"
import React, { useState, useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';
import "../global.css"



export function Filmes() {
  const [filmes, setFilmes] = useState([]);
  const [paginaAtual, setPaginaAtual] = useState(1);

  const filmesPorPagina = 6;
  const totalPaginas = Math.ceil(filmes.length / filmesPorPagina);

  const indiceInicio = (paginaAtual - 1) * filmesPorPagina;
  const indiceFim = paginaAtual * filmesPorPagina;

  const filmesDaPagina = filmes.slice(indiceInicio, indiceFim);

  const handlePaginaAnterior = () => {
      if (paginaAtual > 1) {
          setPaginaAtual(paginaAtual - 1);
      }
  };

  const handlePaginaProxima = () => {
      if (paginaAtual < totalPaginas) {
          setPaginaAtual(paginaAtual + 1);
      }
  };

  useEffect(() => {
      // Fazer solicitação à API e atualizar o estado com os dados recebidos
      fetch('http://localhost:3333/movie')
          .then(response => response.json())
          .then(data => setFilmes(data))
          .catch(error => console.error('Erro ao obter dados da API:', error));
  }, []);
  return (
    <div className={"Background"}>
      <div className={"Background-Opacity"}>
        <div>
          <Header />
            <div>
              <div className={styles.barrapesq}>
                <div className={styles.linhabarra}>
                  <input type="text" placeholder="Pesquisar filmes" />
                  <button><img src="src/Assets/Lupa.svg"/></button>
                </div>
                <div className={styles.linhafiltros}>
                  <select>
                    <option value="">Gênero</option>
                    <option value="Drama">Drama</option>
                    <option value="Ação">Ação</option>
                    <option value="Aventura">Aventura</option>
                    <option value="Comédia">Comédia</option>
                    <option value="Animação">Animação</option>
                    <option value="Fantasia">Fantasia</option>
                    <option value="Ficção">Ficção Científica</option>
                    <option value="Crime">Crime</option>
                  </select>
                  <select>
                    <option value="">Classificação</option>
                    <option value="0">Livre</option>
                    <option value="1">10 anos</option>
                    <option value="2">12 anos</option>
                    <option value="3">14 anos</option>
                    <option value="4">16 anos</option>
                    <option value="5">18 anos</option>
                  </select>
                    
                </div>
              </div>
              <main className={styles.filmes}>
                <div className={styles.wrapper}>
                  <h1>Filmes</h1>                
                  <div className={styles.cards}>
                    {filmesDaPagina.map(filme => (
                      <Card
                        key={uuidv4()} 
                        titulo={filme.titulo}
                        imgurl={filme.imgurl}
                        sinopse={filme.sinopse}
                        genero={filme.genero}
                        classificacao={filme.classificacao}
                        diretor={filme.diretor}
                      />
                    ))}
                </div>
                <div className={styles.pagButtons}>
                  <button onClick={handlePaginaAnterior}>
                    <img src="src/Assets/Anterior.svg" alt="Anterior" />
                  </button>
                  <div className={styles.numButtons}>
                    {Array.from({ length: totalPaginas }, (_, index) => (
                      <button
                        key={index}
                        onClick={() => setPaginaAtual(index + 1)}
                        className={paginaAtual === index + 1 ? styles.pagAtiva : ''}
                      >
                        {index + 1}
                      </button>
                    ))}
                  </div>
                  <button onClick={handlePaginaProxima}>
                    <img src="src/Assets/Proximo.svg" alt="Próximo" />
                  </button>
                  </div>
                </div>
              </main>
            </div>
          <Footer />
        </div>
      </div>
    </div>
  )

}