import React from 'react';
import { Footer } from "../components/Footer";
import { Header } from "../components/Header";
import styles from "./login.module.css"
import "./global.css"
import { NavLink } from 'react-router-dom';


export function Login(){
    return(
        <div>
            <Header />
                <div>
                    <main className={styles.login}>
                        <div className={styles.cinlogo}>
                            <img src="src/Assets/Logo2.svg" />
                            <div className={styles.logotext}>
                                <p>Escolha suas sessões, reserve seus lugares e entre de cabeça em narrativas que cativam e emocionam. Este é o nosso convite para você vivenciar o cinema de maneira única. Nossa página de login é a porta de entrada para essa experiência excepcional, e estamos empolgados para compartilhar cada momento cinematográfico com você.</p>
                            </div>
                        </div>
                        <form className={styles.loginform}>
                            <div className={styles.text}>
                                <h1>Login</h1>
                                <p>Faça login e garanta seu lugar na diversão</p>
                            </div>
                            <div className={styles.input}>
                                <input type="text" name="nome" placeholder="Usuário ou E-mail" />
                                <input type="password" name="password" placeholder="Senha"> 
                                </input>
                            </div>
                            <a href="">Esqueci minha senha</a>
                            <div className={styles.buttons}>
                                <button className={styles.enterbutton}>ENTRAR</button>
                                    <hr></hr>
                                <NavLink to="/register"><button className={styles.cadasbutton}>CADASTRE-SE</button> </NavLink>
                            </div>
                        </form>
                    </main>
                </div>
            <Footer />
        </div>
    )
}
