const sequelize = require("../config/database");
const { DataTypes } = require("sequelize")
const { v4: uuidv4} = require("uuid");

//Usuarios
const User = sequelize.define("User", {
    Userid: {
        type: DataTypes.UUIDV4,
        defaultValue: ()=> uuidv4(),
        primaryKey: true
    },
    nome: {  
        type: DataTypes.STRING,
        allowNull: false,
    },
    sobrenome: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    cpf: {
        type: DataTypes.STRING(11),
        allowNull: false,

    },
    datanasc: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    username: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    email: { 
        type: DataTypes.STRING,
        allowNull: false,
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
    }
})

//Assentos
const Seat = sequelize.define("Seat",{
    numero:{
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    fileira:{
        type: DataTypes.STRING,
        allowNull: false,
    },
    preco:{
        type: DataTypes.FLOAT,
        allowNull: false,
    },
    cpf:{
        type: DataTypes.STRING(11),
        allowNull: false,
    },
    nome: {
        type: DataTypes.STRING,
        allowNull: false,
    }
})
//Sessao
const Session = sequelize.define("Session",{
    Sessionid: {
        type: DataTypes.UUIDV4,
        defaultValue: ()=> uuidv4(),
        primaryKey: true
    },
    tempo:{ 
        type: DataTypes.STRING,
        allowNull: false,
    },
    cidade:{
        type: DataTypes.STRING,
        allowNull:false,
    },
    bairro:{
        type: DataTypes.STRING,
        allowNull:false,
    },
    tipo:{
        type: DataTypes.INTEGER,
        allowNull: false,
    }
})


//Filmes
const Movie = sequelize.define ("Movie", {
    Movieid :{
        type: DataTypes.UUID,
        defaultValue: () => uuidv4(),
        allowNull: false,
        primaryKey: true
    },
    titulo: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    imgurl: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    sinopse: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    genero: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    classificacao: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    diretor: {
        type: DataTypes.STRING,
        allowNull: false,
    }


}, {
    tableName: 'movies',
    modelName: 'Movie', 
})


//Relação (1:N), se filme for deletado, deletar todas suas sessoes

Movie.hasMany(Session, { onDelete: "CASCADE"})

//Relação (1:N)
// se sessao for deletado, deletar todos seus assentos
Session.hasMany(Seat, {onDelete: "CASCADE"})

Seat.belongsTo(Session, {foreignKey: "Sessionid"})

module.exports = {
    User,
    Movie,
    Seat,
    Session
}